# I. Init

## 3. sudo c pa bo

### 🌞 Ajouter votre utilisateur au groupe docker

```
[user@localhost ~]$ sudo usermod -a -G docker user
[user@localhost ~]$ exit
logout
Connection to 192.168.64.19 closed.
radiou22@MacBook-Air-de-jojo Documents % ssh user@192.168.64.19
user@192.168.64.19's password: 
Last login: Thu Dec 21 16:07:44 2023 from 192.168.64.1
[user@localhost ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

## 4. Un premier conteneur en vif

### 🌞 Lancer un conteneur NGINX

```
[user@localhost ~]$ docker run -d -p 9999:80 nginx
Unable to find image 'nginx:latest' locally
latest: Pulling from library/nginx
24e221e92a36: Pull complete 
58cc89079bd7: Pull complete 
3799b53049f3: Pull complete 
2a580edba2f4: Pull complete 
cfe7877ea167: Pull complete 
6f26751fc54b: Pull complete 
c98494bb3682: Pull complete 
Digest: sha256:2bdc49f2f8ae8d8dc50ed00f2ee56d00385c6f8bc8a8b320d0a294d9e3b49026
Status: Downloaded newer image for nginx:latest
a2555057df12c40588f98dade5c41c1b890065ae2b6bde96067e4f0ffcfa5002
```

### 🌞 Visitons

```
[user@localhost ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS                                   NAMES
a2555057df12   nginx     "/docker-entrypoint.…"   7 minutes ago   Up 7 minutes   0.0.0.0:9999->80/tcp, :::9999->80/tcp   interesting_northcutt
[user@localhost ~]$ docker logs interesting_northcutt
/docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
/docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
/docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
10-listen-on-ipv6-by-default.sh: info: Getting the checksum of /etc/nginx/conf.d/default.conf
10-listen-on-ipv6-by-default.sh: info: Enabled listen on IPv6 in /etc/nginx/conf.d/default.conf
/docker-entrypoint.sh: Sourcing /docker-entrypoint.d/15-local-resolvers.envsh
/docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
/docker-entrypoint.sh: Launching /docker-entrypoint.d/30-tune-worker-processes.sh
/docker-entrypoint.sh: Configuration complete; ready for start up
2023/12/21 16:24:43 [notice] 1#1: using the "epoll" event method
2023/12/21 16:24:43 [notice] 1#1: nginx/1.25.3
2023/12/21 16:24:43 [notice] 1#1: built by gcc 12.2.0 (Debian 12.2.0-14) 
2023/12/21 16:24:43 [notice] 1#1: OS: Linux 5.14.0-284.11.1.el9_2.aarch64
2023/12/21 16:24:43 [notice] 1#1: getrlimit(RLIMIT_NOFILE): 1073741816:1073741816
2023/12/21 16:24:43 [notice] 1#1: start worker processes
2023/12/21 16:24:43 [notice] 1#1: start worker process 29
2023/12/21 16:24:43 [notice] 1#1: start worker process 30
2023/12/21 16:24:43 [notice] 1#1: start worker process 31
2023/12/21 16:24:43 [notice] 1#1: start worker process 32
[user@localhost ~]$ docker inspect interesting_northcutt
[
    {
        "Id": "a2555057df12c40588f98dade5c41c1b890065ae2b6bde96067e4f0ffcfa5002",
        "Created": "2023-12-21T16:24:42.818346282Z",
        "Path": "/docker-entrypoint.sh",
        "Args": [
            "nginx",
            "-g",
            "daemon off;"
        ],
        "State": {
            "Status": "running",
            "Running": true,
            "Paused": false,
            "Restarting": false,
            "OOMKilled": false,
            "Dead": false,
            "Pid": 4412,
            "ExitCode": 0,
            "Error": "",
            "StartedAt": "2023-12-21T16:24:43.412108133Z",
            "FinishedAt": "0001-01-01T00:00:00Z"
        },
        "Image": "sha256:8aea65d81da202cf886d7766c7f2691bb9e363c6b5d9b1f5d9ddaaa4bc1e90c2",
        "ResolvConfPath": "/var/lib/docker/containers/a2555057df12c40588f98dade5c41c1b890065ae2b6bde96067e4f0ffcfa5002/resolv.conf",
        "HostnamePath": "/var/lib/docker/containers/a2555057df12c40588f98dade5c41c1b890065ae2b6bde96067e4f0ffcfa5002/hostname",
        "HostsPath": "/var/lib/docker/containers/a2555057df12c40588f98dade5c41c1b890065ae2b6bde96067e4f0ffcfa5002/hosts",
        "LogPath": "/var/lib/docker/containers/a2555057df12c40588f98dade5c41c1b890065ae2b6bde96067e4f0ffcfa5002/a2555057df12c40588f98dade5c41c1b890065ae2b6bde96067e4f0ffcfa5002-json.log",
        "Name": "/interesting_northcutt",
        "RestartCount": 0,
        "Driver": "overlay2",
        "Platform": "linux",
        "MountLabel": "",
        "ProcessLabel": "",
        "AppArmorProfile": "",
        "ExecIDs": null,
        "HostConfig": {
            "Binds": null,
            "ContainerIDFile": "",
            "LogConfig": {
                "Type": "json-file",
                "Config": {}
            },
            "NetworkMode": "default",
            "PortBindings": {
                "80/tcp": [
                    {
                        "HostIp": "",
                        "HostPort": "9999"
                    }
                ]
            },
            "RestartPolicy": {
                "Name": "no",
                "MaximumRetryCount": 0
            },
            "AutoRemove": false,
            "VolumeDriver": "",
            "VolumesFrom": null,
            "ConsoleSize": [
                65,
                208
            ],
            "CapAdd": null,
            "CapDrop": null,
            "CgroupnsMode": "private",
            "Dns": [],
            "DnsOptions": [],
            "DnsSearch": [],
            "ExtraHosts": null,
            "GroupAdd": null,
            "IpcMode": "private",
            "Cgroup": "",
            "Links": null,
            "OomScoreAdj": 0,
            "PidMode": "",
            "Privileged": false,
            "PublishAllPorts": false,
            "ReadonlyRootfs": false,
            "SecurityOpt": null,
            "UTSMode": "",
            "UsernsMode": "",
            "ShmSize": 67108864,
            "Runtime": "runc",
            "Isolation": "",
            "CpuShares": 0,
            "Memory": 0,
            "NanoCpus": 0,
            "CgroupParent": "",
            "BlkioWeight": 0,
            "BlkioWeightDevice": [],
            "BlkioDeviceReadBps": [],
            "BlkioDeviceWriteBps": [],
            "BlkioDeviceReadIOps": [],
            "BlkioDeviceWriteIOps": [],
            "CpuPeriod": 0,
            "CpuQuota": 0,
            "CpuRealtimePeriod": 0,
            "CpuRealtimeRuntime": 0,
            "CpusetCpus": "",
            "CpusetMems": "",
            "Devices": [],
            "DeviceCgroupRules": null,
            "DeviceRequests": null,
            "MemoryReservation": 0,
            "MemorySwap": 0,
            "MemorySwappiness": null,
            "OomKillDisable": null,
            "PidsLimit": null,
            "Ulimits": null,
            "CpuCount": 0,
            "CpuPercent": 0,
            "IOMaximumIOps": 0,
            "IOMaximumBandwidth": 0,
            "MaskedPaths": [
                "/proc/asound",
                "/proc/acpi",
                "/proc/kcore",
                "/proc/keys",
                "/proc/latency_stats",
                "/proc/timer_list",
                "/proc/timer_stats",
                "/proc/sched_debug",
                "/proc/scsi",
                "/sys/firmware",
                "/sys/devices/virtual/powercap"
            ],
            "ReadonlyPaths": [
                "/proc/bus",
                "/proc/fs",
                "/proc/irq",
                "/proc/sys",
                "/proc/sysrq-trigger"
            ]
        },
        "GraphDriver": {
            "Data": {
                "LowerDir": "/var/lib/docker/overlay2/ff6b566822a426f9277af7563bec026023d6421270a6b7922a8a1f1c6f5c31bc-init/diff:/var/lib/docker/overlay2/d6aceec5342c5bf40240b0b4f6ba2c9a82984732530b56d2f4a05597e532cb77/diff:/var/lib/docker/overlay2/4d256f3171a87c00bf53c1b5976cbbba08ec5f91e407844368be2c60196948c7/diff:/var/lib/docker/overlay2/d5e4eda4d4993148799a1880b2ad78de0bb39fcda8e9156b02fa73acd6da1236/diff:/var/lib/docker/overlay2/ac9ff963fe84d3757e0cf3654206dceabd334366f13cb21f5ad86533212230c2/diff:/var/lib/docker/overlay2/0f7a5fe8a1e98f85ea7261525bad8816c23914ec1702ce6e6fcd6a75fd4530bf/diff:/var/lib/docker/overlay2/b566cb8421a29fa1b4a6ba1a2ba37dbacfa039baf38be952a590f570ff84f0ef/diff:/var/lib/docker/overlay2/a0930ed270da6dc07a3a1d25cc248326a9c91a42d2bccd5b8f3a09fa3310c7e5/diff",
                "MergedDir": "/var/lib/docker/overlay2/ff6b566822a426f9277af7563bec026023d6421270a6b7922a8a1f1c6f5c31bc/merged",
                "UpperDir": "/var/lib/docker/overlay2/ff6b566822a426f9277af7563bec026023d6421270a6b7922a8a1f1c6f5c31bc/diff",
                "WorkDir": "/var/lib/docker/overlay2/ff6b566822a426f9277af7563bec026023d6421270a6b7922a8a1f1c6f5c31bc/work"
            },
            "Name": "overlay2"
        },
        "Mounts": [],
        "Config": {
            "Hostname": "a2555057df12",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": false,
            "AttachStderr": false,
            "ExposedPorts": {
                "80/tcp": {}
            },
            "Tty": false,
            "OpenStdin": false,
            "StdinOnce": false,
            "Env": [
                "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
                "NGINX_VERSION=1.25.3",
                "NJS_VERSION=0.8.2",
                "PKG_RELEASE=1~bookworm"
            ],
            "Cmd": [
                "nginx",
                "-g",
                "daemon off;"
            ],
            "Image": "nginx",
            "Volumes": null,
            "WorkingDir": "",
            "Entrypoint": [
                "/docker-entrypoint.sh"
            ],
            "OnBuild": null,
            "Labels": {
                "maintainer": "NGINX Docker Maintainers <docker-maint@nginx.com>"
            },
            "StopSignal": "SIGQUIT"
        },
        "NetworkSettings": {
            "Bridge": "",
            "SandboxID": "302e690071b7b6c6cadcf6052d5c1baac95f9a1e26ee29e91f88e59ce0b0eaa4",
            "HairpinMode": false,
            "LinkLocalIPv6Address": "",
            "LinkLocalIPv6PrefixLen": 0,
            "Ports": {
                "80/tcp": [
                    {
                        "HostIp": "0.0.0.0",
                        "HostPort": "9999"
                    },
                    {
                        "HostIp": "::",
                        "HostPort": "9999"
                    }
                ]
            },
            "SandboxKey": "/var/run/docker/netns/302e690071b7",
            "SecondaryIPAddresses": null,
            "SecondaryIPv6Addresses": null,
            "EndpointID": "820a1d3057a8a3e5d0829cda59f296ace426b6f4532e442a9b960192b9138b83",
            "Gateway": "172.17.0.1",
            "GlobalIPv6Address": "",
            "GlobalIPv6PrefixLen": 0,
            "IPAddress": "172.17.0.2",
            "IPPrefixLen": 16,
            "IPv6Gateway": "",
            "MacAddress": "02:42:ac:11:00:02",
            "Networks": {
                "bridge": {
                    "IPAMConfig": null,
                    "Links": null,
                    "Aliases": null,
                    "NetworkID": "351254bd4e7a81ec8bf721614171d1310a9062b0f9f052d59947b0c5966cffe9",
                    "EndpointID": "820a1d3057a8a3e5d0829cda59f296ace426b6f4532e442a9b960192b9138b83",
                    "Gateway": "172.17.0.1",
                    "IPAddress": "172.17.0.2",
                    "IPPrefixLen": 16,
                    "IPv6Gateway": "",
                    "GlobalIPv6Address": "",
                    "GlobalIPv6PrefixLen": 0,
                    "MacAddress": "02:42:ac:11:00:02",
                    "DriverOpts": null
                }
            }
        }
    }
]
[user@localhost ~]$ sudo ss -lnpt
State                Recv-Q               Send-Q                             Local Address:Port                             Peer Address:Port              Process                                              
LISTEN               0                    128                                      0.0.0.0:22                                    0.0.0.0:*                  users:(("sshd",pid=718,fd=3))                       
LISTEN               0                    4096                                     0.0.0.0:9999                                  0.0.0.0:*                  users:(("docker-proxy",pid=4363,fd=4))              
LISTEN               0                    128                                         [::]:22                                       [::]:*                  users:(("sshd",pid=718,fd=4))                       
LISTEN               0                    4096                                        [::]:9999                                     [::]:*                  users:(("docker-proxy",pid=4368,fd=4)) 
[user@localhost ~]$ sudo firewall-cmd --zone=public --add-port=9999/tcp --permanent
success
[user@localhost ~]$ sudo firewall-cmd --reload
success
```

### 🌞 On va ajouter un site Web au conteneur NGINX

```
[user@localhost ~]$ mkdir nginx
[user@localhost ~]$ cd nginx/
[user@localhost nginx]$ pwd
/home/user/nginx
[user@localhost nginx]$ nano index.html
[user@localhost nginx]$ nano site_nul.conf
[user@localhost nginx]$ nano site_nul.conf
[user@localhost nginx]$ docker run -d -p 9999:8080 -v /home/user/nginx/index.html:/var/www/html/index.html -v /home/user/nginx/site_nul.conf:/etc/nginx/conf.d/site_nul.conf nginx
665440e0494e05fe28b760d3e590ccc358babad83247fb7bcce3fa938da1d51b
docker: Error response from daemon: driver failed programming external connectivity on endpoint gifted_saha (eb93bf99485e266ab9d41d9be72c490d01d3ab4e42dce826f95d0de4af2a311c): Bind for 0.0.0.0:9999 failed: port is already allocated.
[user@localhost nginx]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED          STATUS          PORTS                                   NAMES
a2555057df12   nginx     "/docker-entrypoint.…"   20 minutes ago   Up 20 minutes   0.0.0.0:9999->80/tcp, :::9999->80/tcp   interesting_northcutt
[user@localhost nginx]$ docker stop a2555057df12
a2555057df12
[user@localhost nginx]$ docker run -d -p 9999:8080 -v /home/user/nginx/index.html:/var/www/html/index.html -v /home/user/nginx/site_nul.conf:/etc/nginx/conf.d/site_nul.conf nginx
7d57657d5bcc1faf330af7888aa4db72f5bcb59a9cf740cb1019084d504a8184
[user@localhost nginx]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED          STATUS          PORTS                                               NAMES
7d57657d5bcc   nginx     "/docker-entrypoint.…"   11 seconds ago   Up 10 seconds   80/tcp, 0.0.0.0:9999->8080/tcp, :::9999->8080/tcp   keen_moore
```

### 🌞 Visitons



## 5. Un deuxième conteneur en vif

### 🌞 Lance un conteneur Python, avec un shell

```
[user@localhost nginx]$ docker run -it python bash
Unable to find image 'python:latest' locally
latest: Pulling from library/python
b66b4ecd3ecf: Pull complete 
6c641d36985b: Pull complete 
ddd8544b6e15: Pull complete 
ae58c7c06d64: Pull complete 
f9f35f1c3178: Pull complete 
0d89c447e056: Pull complete 
9862771c91cc: Pull complete 
93aa698c30e4: Pull complete 
Digest: sha256:3733015cdd1bd7d9a0b9fe21a925b608de82131aa4f3d397e465a1fcb545d36f
Status: Downloaded newer image for python:latest
root@dacead68b683:/# python
Python 3.12.1 (main, Dec 19 2023, 16:44:02) [GCC 12.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> hello
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'hello' is not defined. Did you mean: 'help'?
>>> exit()
```

### 🌞 Installe des libs Python

```
root@dacead68b683:/# pip3 install aiohttp

[...]

[notice] A new release of pip is available: 23.2.1 -> 23.3.2
[notice] To update, run: pip install --upgrade pip

root@dacead68b683:/# pip3 install aioconsole
Collecting aioconsole
  Obtaining dependency information for aioconsole from https://files.pythonhosted.org/packages/f7/39/b392dc1a8bb58342deacc1ed2b00edf88fd357e6fdf76cc6c8046825f84f/aioconsole-0.7.0-py3-none-any.whl.metadata
  Downloading aioconsole-0.7.0-py3-none-any.whl.metadata (5.3 kB)
Downloading aioconsole-0.7.0-py3-none-any.whl (30 kB)
Installing collected packages: aioconsole
Successfully installed aioconsole-0.7.0
WARNING: Running pip as the 'root' user can result in broken permissions and conflicting behaviour with the system package manager. It is recommended to use a virtual environment instead: https://pip.pypa.io/warnings/venv
[notice] A new release of pip is available: 23.2.1 -> 23.3.2
[notice] To update, run: pip install --upgrade pip

root@dacead68b683:/# python 
Python 3.12.1 (main, Dec 19 2023, 16:44:02) [GCC 12.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import aiohttp
>>> exit()
```

# II. Images

## 1. Images publiques

### 🌞 Récupérez des images
```
[user@localhost nginx]$ docker pull linuxserver/wikijs
Using default tag: latest
latest: Pulling from linuxserver/wikijs
e03d91f524e7: Pull complete 
07a0e16f7be1: Pull complete 
24b296671cef: Pull complete 
332c019da471: Pull complete 
a3c9e8b6dce0: Pull complete 
030507257325: Pull complete 
1f9aa5cff2b1: Pull complete 
Digest: sha256:131d247ab257cc3de56232b75917d6f4e24e07c461c9481b0e7072ae8eba3071
Status: Downloaded newer image for linuxserver/wikijs:latest
docker.io/linuxserver/wikijs:latest

user@localhost nginx]$ docker images
REPOSITORY           TAG       IMAGE ID       CREATED       SIZE
mysql                latest    8e409b83ace6   2 days ago    641MB
linuxserver/wikijs   latest    772abe6ffca7   6 days ago    459MB
python               latest    b3148b8eb04d   13 days ago   1.02GB
wordpress            latest    4468dc043c37   2 weeks ago   739MB
python               3.11      f983f601e9a2   2 weeks ago   1.01GB
nginx                latest    8aea65d81da2   8 weeks ago   192MB
```

### 🌞 Lancez un conteneur à partir de l'image Python

```
[user@localhost nginx]$ docker run -it python bash
root@2044ae0c1f5c:/# python
Python 3.12.1 (main, Dec 19 2023, 16:44:02) [GCC 12.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> exit()
```

## 2. Construire une image

### 🌞 Ecrire un Dockerfile pour une image qui héberge une application Python

```
[user@localhost ~]$ mkdir app
[user@localhost ~]$ cd app
[user@localhost app]$ pwd
/home/user/app
[user@localhost app]$ nano app.py
[user@localhost app]$ cd ..
[user@localhost ~]$ ls
app  nginx
[user@localhost ~]$ cd app/
[user@localhost app]$ ls
Dockerfile  app.py
```

### 🌞 Build l'image

```
[user@localhost app]$ docker build . -t smoothi:noob
[+] Building 0.4s (10/10) FINISHED                                                                                                                                                               docker:default
 => [internal] load .dockerignore                                                                                                                                                                          0.0s
 => => transferring context: 2B                                                                                                                                                                            0.0s
 => [internal] load build definition from Dockerfile                                                                                                                                                       0.0s
 => => transferring dockerfile: 247B                                                                                                                                                                       0.0s
 => [internal] load metadata for docker.io/library/debian:latest                                                                                                                                           0.4s
 => [1/5] FROM docker.io/library/debian@sha256:bac353db4cc04bc672b14029964e686cd7bad56fe34b51f432c1a1304b9928da                                                                                            0.0s
 => [internal] load build context                                                                                                                                                                          0.0s
 => => transferring context: 86B                                                                                                                                                                           0.0s
 => CACHED [2/5] RUN apt update                                                                                                                                                                            0.0s
 => CACHED [3/5] RUN apt install python3 python3-emoji -y                                                                                                                                                  0.0s
 => CACHED [4/5] RUN mkdir ./app                                                                                                                                                                           0.0s
 => CACHED [5/5] COPY app.py /app/app.py                                                                                                                                                                   0.0s
 => exporting to image                                                                                                                                                                                     0.0s
 => => exporting layers                                                                                                                                                                                    0.0s
 => => writing image sha256:50c097fcd5e0e90741cb7b2b9a49c22f738eab0e8ed181fc77054844812019af                                                                                                               0.0s
 => => naming to docker.io/library/smoothi:noob                                                                                                                                                            0.0s 

[user@localhost app]$ cat Dockerfile 
FROM debian
RUN apt update
RUN apt install python3 python3-emoji -y 
RUN mkdir ./app
COPY app.py /app/app.py


ENTRYPOINT ["python3", "/app/app.py"]
```

### 🌞 Lancer l'image 

```
[user@localhost app]$ docker run smoothi:noob
Cet exemple d'application est vraiment naze 👎
```

# III. Docker compose

### 🌞 Créez un fichier docker-compose.yml

```
[user@localhost ~]$ mkdir compose
[user@localhost ~]$ cd compose/
[user@localhost compose]$ nano docker-compose.yml

```

### 🌞 Lancez les deux conteneurs avec docker compose
```
[user@localhost compose]$ docker compose up -d
[+] Running 3/3
 ✔ conteneur_flopesque Pulled                                                                                                                                                                              2.9s 
 ✔ conteneur_nul 1 layers [⣿]      0B/0B      Pulled                                                                                                                                                       2.6s 
   ✔ b66b4ecd3ecf Already exists                                                                                                                                                                           0.0s 
[+] Running 3/3
 ✔ Network compose_default                  Created                                                                                                                                                        0.3s 
 ✔ Container compose-conteneur_flopesque-1  Started                                                                                                                                                        0.0s 
 ✔ Container compose-conteneur_nul-1        Started 
```

### 🌞 Vérifier que les deux conteneurs tournent

```

```